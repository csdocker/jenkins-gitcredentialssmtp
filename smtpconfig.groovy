import jenkins.model.*
import hudson.tasks.*

// Setting admin email
def env = System.getenv()
String adminAddress = env['adminAddress']

JenkinsLocationConfiguration config = GlobalConfiguration.all().get(JenkinsLocationConfiguration)
config.setAdminAddress(adminAddress)
config.save()

//Setting Email to send notifications
String SMTPPort = env['SMTPPort']
String SMTPPass = env['SMTPPass']
String SMTPServer = env['SMTPServer']
String SMTPUser = env['SMTPUser']
String SMTPReplyTo = env['SMTPReplyTo']

def descriptor = hudson.tasks.Mailer.descriptor()

descriptor.setSmtpHost(SMTPServer)
descriptor.setSmtpPort(SMTPPort)
descriptor.setSmtpAuth(SMTPUser, SMTPPass)
descriptor.setReplyToAddress(SMTPReplyTo)
  
descriptor.save()

